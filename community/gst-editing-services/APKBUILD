# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gst-editing-services
pkgver=1.24.4
pkgrel=0
pkgdesc="GStreamer Editing Services Library"
url="https://gstreamer.freedesktop.org"
# s390x blocked by 7 failing tests
arch="all !s390x"
license="LGPL-2.0-or-later"
makedepends="
	flex
	glib-dev
	gobject-introspection-dev
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gst-plugins-good
	gstreamer-dev
	gtk-doc
	libxml2-dev
	meson
	py3-gobject3-dev
	python3
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://gstreamer.freedesktop.org/src/gst-editing-services/gst-editing-services-$pkgver.tar.xz"
options="!check" # https://gitlab.freedesktop.org/gstreamer/gst-editing-services/-/issues/125

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dtests="$(want_check && echo enabled || echo disabled)" \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
f427ee72ca3fa36625486702ecd97ec6a048293d877ec8f5e747c103b530b3ee4b8f7cd100a54f48ead48cfdac99455878590efc5aae7c8d6e3a297c28b5ccf5  gst-editing-services-1.24.4.tar.xz
"

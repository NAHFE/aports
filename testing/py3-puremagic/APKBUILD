# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-puremagic
pkgver=1.24
pkgrel=0
pkgdesc="Pure python implementation of identifying files based off their magic numbers"
url="https://github.com/cdgriffith/puremagic"
arch="noarch"
license="MIT"
makedepends="py3-setuptools py3-gpep517 py3-wheel"
checkdepends="py3-pytest py3-pytest-cov"
subpackages="$pkgname-pyc"
source="https://github.com/cdgriffith/puremagic/archive/refs/tags/$pkgver/puremagic-$pkgver.tar.gz"
builddir="$srcdir/puremagic-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	gpep517 install-wheel --destdir .testenv --prefix '' .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	gpep517 install-wheel --destdir "$pkgdir" \
		.dist/*.whl
}

sha512sums="
06ef1008788c2941df9b3237d41df9908bf172e22f66e478470ab66d3cefd0c8d598f8a95df285bfde58477a98d4da557c0fef7a691b30bcf3d82d76067c518d  puremagic-1.24.tar.gz
"
